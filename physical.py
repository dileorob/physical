from numpy import *
import pandas as pd

datafile = "lectures_data.xlsx"

weekdays = {'LUN': 0, 'MAR': 1, 'MER': 2, 'GIO': 3, 'VEN': 4}
header = open('html/header.html', 'r').read()
toc1 = open('html/toc1.html', 'r').read()
toc2 = open('html/toc2.html', 'r').read()
footer = open('html/footer.html', 'r').read()



def parse(slot):
    tokens = slot.split()
    if len(tokens)==2:
        daystr, hourstr = tokens
        weekstr = ""
    elif len(tokens)==3:
        daystr, hourstr, weekstr = tokens

    day = weekdays[daystr]
    h1, h2 = hourstr.split('-')
    hours = arange(int(h1), int(h2))

    if weekstr:
        w1, w2 = weekstr.split('-')
        weeks = arange(int(w1), int(w2)+1)
    else:
        weeks=arange(WEEKS)

    return day, hours, weeks


def overlap(slot1, slot2):
    day1, h1, w1 = parse(slot1)
    day2, h2, w2 = parse(slot2)

    ret=False

    if day1==day2:
        if set(h1).intersection(h2) and set(w1).intersection(w2):
            ret = True

    return ret

def course_by_name(name):
    for course in courses.values():
        if course.name == name:
            return course

class Group:
    def __init__(self, name, channels=None, students=None):
        self.name = name
        self.channels = channels
        self.students = students


    def __repr__(self):
        return self.name


class Course:
    def __init__(self, name, group, hours, teachers, tags):
        self.name = name
        self.group = group
        self.hours = hours
        self.teachers = teachers
        self.tags = tags

    def add_lectures(self, slots, classrooms, channel=None, comment=""):

        if not channel:
            for room, chan, teacher in zip(classrooms, groups[self.group].channels, self.teachers):
                for slot in slots:
                    rooms[room].allocate(slot, self.name, chan, teacher, comment=comment)
        else:
            for slot in slots:
                rooms[classrooms[0]].allocate(slot, self.name, groups[self.group].channels[channel-1], self.teachers[channel-1], comment=comment)


    def __repr__(self):
        return self.name

class Classroom:
    def __init__(self, name, seats):
        self.name = name
        self.seats = seats
        self.cal = zeros((5,12,WEEKS), dtype=str)
        self.slots=[]
        self.courses=[]
        self.comments=[]


    def allocate(self, slot, coursename, chan, teacher, comment):
        if self.isavailable(slot):
            self.slots += [slot]
            self.courses += ["%s,%s,%s" % (coursename, chan, teacher)]
            self.comments += [comment]

    def isavailable(self, newslot):
        for slot in self.slots:
            if overlap(newslot, slot):
                print("Room %s is busy at %s" % (self.name, newslot))
                return False
        return True

    def makematrix(self):
        matrix = [["" for i in range(5)] for i in range(12)]
        #matrix = zeros((12,5), dtype=object)
        for slot, course, comment in zip(self.slots, self.courses, self.comments):
            day, hours, weeks = parse(slot)
            for hour in hours:
                name, chan, teacher = course.split(',')
                if matrix[hour-8][day]:
                    matrix[hour-8][day]+="<br><br>"
                groupclass = course_by_name(name).group.split('.')[0]
                fcomment=comment
                if fcomment != '':
                    fcomment="("+comment+")"
                if chan=='':
                    matrix[hour-8][day] += '<span class="%s">%s %s</span><br> %s' % (groupclass, name, fcomment, teacher)
                else:
                    matrix[hour-8][day] += '<span class="%s">%s</span><br>(%s)<sub>%s</sub> %s' % (groupclass, name, chan, comment, teacher)

                if len(weeks)<WEEKS:
                    matrix[hour-8][day]+="<br><small>dal %s al %s</small>" % (weekday1[weeks[0]-1], weekday5[weeks[-1]-1])
        return matrix


#    def render(self):
#        return HTML(self.tohtml())

    def __repr__(self):
        if isnan(self.seats):
            seats = "unknown"
        else:
            seats = str(self.seats)

        return "%s, capienza: %s" % (self.name, seats)

def byroom(room):
    html = "<a name=\"%s\"><h3>%s</h3></a>" % (room.name, room.name)
    #html+='<table style = "width: 500px"><tr><td class = "legend" style="background-color: #0074d9;">Triennale I</td><td class = "legend" style="background-color: #ff851b;">Triennale II</td><td class = "legend" style="background-color: #ff4136;">Triennale III</td><td class = "legend" style="background-color: #3d9970;">Magistrale I</td><td class = "legend" style="background-color: #b10dc9;">Magistrale II</td></tr></table><br>'
    html+='<table style = "width: 500px"><tr>'
    html+='<td class = "legend"><span class="T1">Triennale I</span></td>'
    html+='<td class = "legend"><span class="T2">Triennale II</span></td>'
    html+='<td class = "legend"><span class="T3">Triennale III</span></td>'
    html+='<td class = "legend"><span class="M1">Magistrale I</span></td>'
    html+='<td class = "legend"><span class="M2">Magistrale II</span></td>'
    html+='</tr></table>'
    html+= '<table class="classroom"><tr>'
    html+="<th> ora </th>"

    matrix = room.makematrix()

    for day in weekdays.keys():
        html+="<th> %s </th>" % day

    for i in range(12):
        html+="<tr><td>%d-%d</td>" % (i+8, i+9)
        for j in range(5):
            html+="<td>"+matrix[i][j]+"</td>"
    html+= "</tr></table>"


    return html

def bygroup(grouplist, title=None):
    if len(grouplist)==1:
        title = groups[grouplist[0]].name

    html = "<a name=%s><h3>%s</h3></a>\n" % (grouplist[0],title)
    html+= '<table class="studgroup"><tr>'
    html+="<th> insegnamento </th><th> docente </th>"
    for day in weekdays.keys():
        html+="<th> %s </th>" % day
    html+="</tr>"

    courses2 = courses_by_group(grouplist)

    for c in courses2:
        for chan in groups[c.group].channels:
            if chan != '':
                coursedescr = "%s (%s)" % (c.name, chan)
            else:
                coursedescr = c.name

            html+="<tr>"
            html+="<td>"+coursedescr+"</td>"
            html+="<td>"+c.teachers[groups[c.group].channels.index(chan)]+"</td>"
            row = [""]*5
            for room in rooms.values():
                for slot, course, comment in zip(room.slots, room.courses, room.comments):
                    tokens = course.split(",")
                    if tokens[0] == c.name and tokens[1]==chan:
                        day, hours, weeks = parse(slot)
                        if row[day] != "":
                            row[day]+="<br>"
                        if comment == '':
                            row[day]+="%s <br>%d-%d" % (room.name, hours[0], hours[-1]+1)
                        else:
                            row[day]+="%s (%s)<br>%d-%d" % (room.name, comment, hours[0], hours[-1]+1)
                        if len(weeks)<WEEKS:
                            row[day]+="<br><small>dal %s<br>al %s</small>" % (weekday1[weeks[0]-1], weekday5[weeks[-1]-1])

            for i in range(5):
                html+="<td>"+row[i]+"</td>"

        html+="</tr>"
    html+="</table>\n"
    return html

def weekplan_bygroup(grouplist, tags=[]):
    matrix = [["" for i in range(5)] for i in range(12)]
    for room in rooms.values():
        for slot,course in zip(room.slots, room.courses):
            day, hours, weeks = parse(slot)
            name, chan, teacher = course.split(',')
            c = course_by_name(name)
            if c.group not in grouplist:
                continue
            if tags and not set(c.tags).intersection(tags):
                continue
            for hour in hours:
                if matrix[hour-8][day]:
                    matrix[hour-8][day]+="<br><br>*"
                groupclass = course_by_name(name).group.split('.')[0]
                if chan=='':
                    matrix[hour-8][day] += '<span class="%s">%s</span>' % (groupclass, name)
                else:
                    matrix[hour-8][day] += '<span class="%s">%s</span> (%s)' % (groupclass, name, chan)

                if len(weeks)<WEEKS:
                    matrix[hour-8][day]+="<br><small>dal %s al %s</small>" % (weekday1[weeks[0]-1], weekday5[weeks[-1]-1])


    html="<table>"
    html+="<th> ora </th>"
    for day in weekdays.keys():
        html+="<th> %s </th>" % day

    for i in range(12):
        html+="<tr><td>%d-%d</td>" % (i+8, i+9)
        for j in range(5):
            if '*' in matrix[i][j]:
                html+='<td style="background-color: #ffff88">'+matrix[i][j]+"</td>"
            else:
                html+="<td>"+matrix[i][j]+"</td>"
    html+= "</tr></table>"

    return html


def courses_by_group(group):
    courses2 = []
    for c in courses.values():
        if groups[c.group.split('.')[0]] == group:
            courses2+= [c]
    return courses2

def courses_by_group(grouplist):
    courses2 = []
    for c in courses.values():
        if c.group in grouplist:
            courses2+= [c]
    return courses2

#def get_group_short(coursename):
#    for course in courses.values():
#        if course.name == coursename:
#            groupname=course.group
#    tokens =  groupname.split()
#    return tokens[0][0]+tokens[1][0]


def count_hours(course, chan):
    h=0
    for room in rooms.values():
        for slot, coursedescr in zip(room.slots, room.courses):
            tokens = coursedescr.split(",")
            if tokens[0] == course.name and tokens[1]==chan:
                day, hours, weeks = parse(slot)
                h+=len(hours)*len(weeks)
    return h

def course_hours():
    html="<table>"
    for course in courses.values():
        channels = groups[course.group].channels
        for chan in channels:
            teacher = course.teachers[channels.index(chan)]
            html+="<tr><td>%s</td> <td>%s</td> <td>%s</td>" % (course.name, chan, teacher)
            hours = count_hours(course, chan)
            #if hours<course.hours:
            #    html+='<td style="background: #ff0000">'
            #else:
            #    html+='<td>'
            html+="<td>%d/%d</td></tr>" % (hours, course.hours)
    html+="</table>"
    return html


def load_groups():
    df = pd.read_excel(datafile, sheet_name="Groups")
    group = {}
    for data in df.values:
        if data[2] is not nan:
            group[data[0]] = Group(name=data[1], channels=data[2].split(","),
                                   students=[int(x) for x in data[3].split(",")])
        else:
            group[data[0]] = Group(name=data[1], channels=[""], students=int(data[3]))
    return group


def load_classrooms(show=False):
    df = pd.read_excel(datafile, sheet_name="Classrooms")
    if show:
        display(df)
    classroom = {}
    for data in df.values:
        if isnan(data[2]):
            seats = nan
        else:
            seats=int(data[2])
        classroom[data[0]] = Classroom(name=data[0], seats=seats)
    return classroom


def load_courses(sem=1):
    #display(df)
    course = {}

    if sem==1:
        df = pd.read_excel(datafile, sheet_name="Courses I sem")
    else:
        df = pd.read_excel(datafile, sheet_name="Courses II sem")

    for data in df.values:
        if data[3] is nan:
            tags = []
        else:
            tags = data[3].split(",")
        course[data[0]] = Course(name=data[1], group=data[2], teachers=data[4].split(","), hours=data[5], tags=tags)

    return course


def export_html(filename, sem=SEMESTER):
    html=""

    html+=bygroup(['T1', 'T1.C'], title=groups['T1'].name)
    html+=bygroup(['T2', 'T2.A'], title=groups['T2'].name)
    html+=bygroup(['T3','T3.O','T3.A'], title=groups['T3'].name)
    html+=bygroup(['M1'])
    if sem==1:
        html+=bygroup(['M2'])

    for room in rooms.values():
        html+=byroom(room)

    f=open(filename, "w")

    if sem==1:
        toc=toc1
    else:
        toc=toc2

    f.write(header+toc+html+footer)
    f.close()

def report(filename):
    html=header+"<h3>Hour count</h3>"
    html+=course_hours()

    html+="<h3>T1</h3>"
    html+=weekplan_bygroup(['T1'])

    html+="<h3>T2</h3>"
    html+=weekplan_bygroup(['T2'])

    html+="<h3>T3, T3.A, T3.O</h3>"
    html+=weekplan_bygroup(['T3', 'T3.A', 'T3.O' ])

    html+="<h3>M1, T,M,B</h3>"
    html+=weekplan_bygroup(['M1'], tags=['T', 'B', 'O'])

    html+="<h3>M1, T,A,P</h3>"
    html+=weekplan_bygroup(['M1'], tags=['T', 'A', 'P', 'O'])

    html+="<h3>M2, T,M,B</h3>"
    html+=weekplan_bygroup(['M2'], tags=['T', 'B'])

    html+="<h3>M2, T,A,P</h3>"
    html+=weekplan_bygroup(['M2'], tags=['T', 'A', 'P'])
    html+=footer

    f=open(filename, "w")
    f.write(html)
    f.close()

groups = load_groups()
rooms=load_classrooms()
courses = load_courses(SEMESTER)
