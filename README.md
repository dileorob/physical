# physical

Routines to help prepare the schedule of lessons and exams at the Physics Department of the Sapienza University of Rome.

To use `physical` routines you need a working Python 3 installation and a Google account.


## Lectures calendar
To prepare a lecture calendar you first need to update the worksheet `lectures_data.xlsx`. You can also upload it on your Google Drive and collaboratively edit it as a Google Sheet but at the end you need to download it in your working directory as an .xlsx file.
The worksheet contains four main Sheets:

1. **Courses I sem** : 1st semester courses list
1. **Courses II sem**: 2nd semester courses list
1. **Classrooms**: list of classrooms
1. **Groups**: student groups


Sheets **Courses I sem** and **Courses II sem** have the following headings:

1. **short name**: a short name to use as a reference the course (avoid spaces).
1. **full name**: the full course name to be displayed on the calendar
1. **student group**: must be one of the student groups specified in the sheet **Groups**.
1. **tags**: tags are mainly used for courses of *Laurea Magistrale* to help identifying possible overlaps between courses within the same interest group.  
Tag values: **A** - Astrophysics, **C** - Condensed matter, **T** - Theory, **P** - Particles, **B** - Biosystems, **O**-?
1. **teacher**: teacher names as displayed in the calendar. The number of teachers must match the number of channels in the specified student group.

See the notebook [1819II.ipynb](1819II.ipynb) for an example of how to allocate hours and classrooms to courses and finally export the calendar in html format for online publication. 

## Exams calendar

For every year you need a separate copy of [this Google Spreadheet](https://docs.google.com/spreadsheets/d/1xE-3dZvW2s0ZMT3rdf0xKhr4-NhXb4auAX8pA9RzpIM/edit?usp=sharing). Rename the spreadsheet preserving the format
**Esami-triennale_YYYY/YYYY** and replacing **YYYY/YYYY** with the current academic year, e.g. **2018/2019**.
The first column has conditional formatting and changes color according to the year number (1,2 or 3). Do not change background color manually. The calendar can be exported in html format for online publishing using the function **Export as HTML** under the menu **Exams**. The exported html will be saved in your Google Drive home directory.
